## 1

### Última vez que usé RTFM y LMGTFY:

Leer la documentación y buscar en Google es parte de mi rutina diaria. Siempre que tengo una duda, busco la respuesta en
la documentación oficial o en Google.
Pero nunca he usado LMGTFY, ya que según envestigué, es una forma sarcástica de decirle a alguien que busque en Google.
Yo pienso que lo correcto es ayudar a las personas a encontrar la respuesta, no burlarse de ellas. Claro teniendo en
cuenta que él o ella ya haya intentado buscar la respuesta por sí mismo.

### Sistema Operativo que uso:

Actualmente uso archi linux con la distribución Manjaro y MacOS.

### Lenguajes que domino:

- Python
    - Django
- Javascript
    - Node
    - Vuejs
    - Reactjs
- Go
    - Go y gRPC (Microservices)