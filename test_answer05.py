import unittest

from answer05 import decode_message


class TestDecodeMessage(unittest.TestCase):
    def test_first_decode_message(self):
        message = "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c206167726567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220617175ed212e"
        expected_message = 'Este es el último paso, por favor, agregame al hangout: \r\n\r\n"martin@mendozadelsolar.com" para saber que llegaste a esta parte.\r\n\r\nGracias, y espero verte por aquí!.'
        response = decode_message(message)
        print(f'\n{response}')
        self.assertEqual(response, expected_message)

    def test_second_decode_message(self):
        message = "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu"
        expected_message = "Si llegas hasta esta parte, por favor, comentamelo con un mensaje."
        response = decode_message(message)
        print(f'\n{response}')
        self.assertEqual(response, expected_message)

    def test_not_hexadecimal_message(self):
        invalid_message = "Este mensaje no está codificado en ASCII hexadecimal ni base64"
        with self.assertRaises(ValueError):
            decode_message(invalid_message)


if __name__ == '__main__':
    unittest.main()
