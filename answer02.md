# CI - Continuous Integration

## Que es CI?

Es una práctica de desarrollo dondé los desarrolladores integran su código en un repositorio compartido varias veces al
día. Cada integración se verifica automáticamente mediante la creación de una compilación y ejecución de pruebas.

## Por qué es importante CI?

Es importante porque permite a los equipos de desarrollo detectar y corregir problemas de integración de código antes de
que se conviertan en problemas mayores.

## Herramientas de CI

- Jenkins
- Gitlab CI
- Github Actions
- Azure DevOps
- AWS CodePipeline

## Ejemplo de CI en Gitlab con python

```yaml
image: python:3.12

stages:
  - test
  - build

test:
  stage: test
  script:
    - echo "Testing the app"
    - pip install -r requirements.txt
    - python -m unittest discover

build:
  stage: build
  script:
    - echo "Building the app"
```


