import base64


def decode_message(encoded_message, encoding='utf-8'):
    ascii_message = encoded_message.replace(">", "").replace("_", "")

    try:
        decoded_bytes = bytes.fromhex(ascii_message)
    except ValueError:
        try:
            decoded_bytes = base64.b64decode(encoded_message)
        except:
            raise ValueError("El mensaje no está en formato ASCII hexadecimal ni base64")

    try:
        decoded_message = decoded_bytes.decode(encoding)
    except UnicodeDecodeError:
        decoded_message = decoded_bytes.decode('latin-1')

    return decoded_message
