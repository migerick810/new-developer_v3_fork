# Projects

## SUNASS

Está es una aplicación que desarrollamos con un equipo de tres de manera independiente del cual yo me encargué de todo
el backend en Django Rest Framework.
Consistía en en registro de indicadores cuando salían del estado en el que se encontraban los servicios de saneamiento.
Para luego brindar reportes del resultado.

![img.png](img.png)

## CUOTA FAMILIAR

Esté projecto también me encargué de todo el backend también desarrollado en Django REST Framework, que consitía en
calcular una cuota de pago por los servicios de agua en los centros poblados existentes en Cajamarca - Perú.

## Authenticacion por cookie con golang y gRPC

Es un proyecto que muestro como se puede autenticar un usuario por medio de cookies con golang y gRPC.
Adjunto el repositorio [aquí](https://github.com/migerick/cookie-authentication-go)