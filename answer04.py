def find_matching_pair(numbers, sum_element):
    if not isinstance(numbers, list):
        raise ValueError("numbers is not a list")

    if not all(isinstance(num, int) for num in numbers):
        raise ValueError("numbers is not a list of integers")

    if not isinstance(sum_element, int):
        raise ValueError("sum_element is not an integer")

    num_set = set()
    numbers = sorted(numbers)

    for num in numbers:
        complement = sum_element - num
        if complement in num_set:
            return f'OK, matching pair:  ({complement}, {num})'
        num_set.add(num)

    return 'No matching pair found'
