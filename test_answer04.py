import unittest

from answer04 import find_matching_pair


class TestFindMatchingPair(unittest.TestCase):
    def test_matching_pair_found(self):
        numbers = [2, 3, 6, 7]
        target_sum = 9
        matching = find_matching_pair(numbers, target_sum)
        print(f'\n{matching}')
        self.assertEqual(matching, 'OK, matching pair:  (3, 6)')

    def test_matching_pair_found_with_repeated_numbers(self):
        numbers = [2, 3, 2, 3, 4, 1]
        target_sum = 5
        matching = find_matching_pair(numbers, target_sum)
        print(matching)
        self.assertEqual(matching, 'OK, matching pair:  (2, 3)')

    def test_no_matching_pair_found(self):
        numbers = [1, 3, 3, 7]
        target_sum = 9
        matching = find_matching_pair(numbers, target_sum)
        print(matching)
        self.assertEqual(matching, 'No matching pair found')

    def test_numbers_not_list(self):
        numbers = 123
        target_sum = 9
        with self.assertRaises(ValueError):
            find_matching_pair(numbers, target_sum)

    def test_numbers_not_list_of_integers(self):
        numbers = [1, 3, 3, "a"]
        target_sum = 9
        with self.assertRaises(ValueError):
            find_matching_pair(numbers, target_sum)

    def test_sum_not_integer(self):
        numbers = [1, 3, 3, 7]
        target_sum = "a"
        with self.assertRaises(ValueError):
            find_matching_pair(numbers, target_sum)

    def test_empty_list(self):
        numbers = []
        target_sum = 9
        self.assertEqual(find_matching_pair(numbers, target_sum), 'No matching pair found')


if __name__ == '__main__':
    unittest.main()
